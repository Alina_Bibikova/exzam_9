import React from 'react';
import {Card, CardBody, CardImg, CardText} from "reactstrap";

const Contact = props => {
    return (
        <Card style={{marginBottom: '20px'}} onClick={props.purchase}>

            <CardBody>
                <CardImg src={props.photo} alt="Photo"/>
                <CardText><strong>{props.name}</strong></CardText>
            </CardBody>
        </Card>
    );
};

export default Contact;