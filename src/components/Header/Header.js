import React from 'react';
import {Container, Navbar, NavbarBrand} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const Header = () => (
    <Navbar color="dark" dark expand="md">
        <Container>
            <NavbarBrand tag={RouterNavLink} to="/" exact>Contacts</NavbarBrand>
        </Container>
    </Navbar>
);


export default Header;