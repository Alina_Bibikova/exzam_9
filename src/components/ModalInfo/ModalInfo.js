import React from 'react';
import {Button, Card, CardBody, CardImg, CardText} from "reactstrap";

const ModalInfo = props => {
    return (
        <Card style={{marginBottom: '20px'}} key={props.id}>
            <CardBody>
                <CardImg src={props.photo} alt="Photo"/>
                <CardText><strong>{props.name}</strong></CardText>
                <CardText><strong>{props.email}</strong></CardText>
                <CardText><strong>{props.phone}</strong></CardText>
                <Button
                    style={{margin: '5px'}}
                    color="info"
                    onClick={props.clickedEdit}
                >
                    Edit
                </Button>
                <Button
                    color="danger"
                    onClick={props.clickedRemove}
                >
                    Delete
                </Button>
            </CardBody>
        </Card>
    );
};

export default ModalInfo;