import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {addContact} from "../../store/actions/contactsAction";
import {connect} from "react-redux";

class ContactForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            phone: '',
            email: '',
            photo: '',
            photoPreview: '',
            edit: false,
        };

        if (this.props.match.params.id) {
            this.state.edit = true;
        }
    }

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value});
    };

    submitHandler = event => {
        event.preventDefault();

        if (this.state.name && this.state.phone && this.state.email && this.state.photo && this.state.photoPreview) {
            this.props.addContact(this.state, this.props.history);
        } else {
            alert('All fields required!');
        }
    };


    render() {
        return (
            <Fragment>
                <div className="page-top clearfix">
                    <h2 className="float-left">{this.state.edit ? 'Edit contact' : 'Add new contact'}</h2>
                </div>

                <Form onSubmit={this.submitHandler}>
                    <FormGroup row>
                        <Label sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input type="text" name="name" id="name" placeholder="name"
                                   value={this.state.name}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Phone</Label>
                        <Col sm={10}>
                            <Input type="number" name="phone" placeholder="phone"
                                   value={this.state.phone}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Email</Label>
                        <Col sm={10}>
                            <Input type="email" name="email" placeholder="email"
                                   value={this.state.email}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Photo</Label>
                        <Col sm={10}>
                            <Input type="url" name="photo" placeholder="photo"
                                   value={this.state.photo}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Photo preview</Label>
                        <Col sm={10}>
                            <Input type="url" name="photoPreview" placeholder="photo preview"
                                   value={this.state.photoPreview}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button color="primary"
                                    type="submit">
                                Save
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>

        );
    }
}

const mapDispatchToProps = dispatch => ({
    addContact: (contact, history) => dispatch(addContact(contact, history)),
});

export default connect(null, mapDispatchToProps)(ContactForm);