import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Row} from "reactstrap";
import Loader from "../../components/UI/Loader/Loader";
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";
import {editContact, fetchContacts, removeContact} from "../../store/actions/contactsAction";
import Modal from "../../components/UI/Modal/Modal";
import Contact from "../../components/Contact/Contact";
import ModalInfo from "../../components/ModalInfo/ModalInfo";

class Contacts extends Component {
    state = {
        purchasing: false
    };

    componentDidMount () {
        this.props.fetchContacts()
    }

    convertToArr = obj => {
        return Object.keys(obj).map(id => {
            return {...obj[id], id};
        });
    };

    purchase = () => {
        this.setState({purchasing: true})
    };

    purchaseCancel = () => {
        this.setState({purchasing: false});
    };

    render() {
        if (this.props.error) {
            return (
                <Alert color="danger">
                    {this.props.error}
                </Alert>
            )
        }

        let contacts = (
            <Row>
            {this.convertToArr(this.props.contacts).map(contact => (
                <Col xs="12" md="4">
                    <Contact
                        {...contact}
                        togleModal={this.state.purchasing}
                        purchase={(() => this.purchase())}
                    />
                </Col>
                ))}
            </Row>
        );

        return (
            <Fragment>
                <Modal
                    show={this.state.purchasing}
                    close={this.purchaseCancel}
                >
                    <ModalInfo
                        name={this.props.contacts.name}
                        phone={this.props.contacts.phone}
                        email={this.props.contacts.email}
                        photo={this.props.contacts.photo}
                        clickedRemove={() => this.props.removeContact()}
                        clickedEdit={() => this.props.editContact()}

                    />
                </Modal>
                <div className="page-top clearfix">
                    <h2 className="float-left">Contacts</h2>
                    <RouterNavLink to={'/contacts/add'} className="float-right">
                        <Button color="info">
                            Add new contact
                        </Button>
                    </RouterNavLink>
                </div>
                {this.props.loading ? <Loader /> : contacts}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    contacts: state.contacts,
    loading: state.loading,
    error: state.error,
});

const mapDispatchToProps = dispatch => ({
    fetchContacts: () => dispatch(fetchContacts()),
    removeContact: id => dispatch(removeContact(id)),
    editContact: id => dispatch(editContact(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);