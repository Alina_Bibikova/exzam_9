import React, { Component } from 'react';
import {Route, Switch} from "react-router";
import Layout from "./components/Layout/Layout";
import './App.css';
import Contacts from "./containers/Contacts/Contacts";
import ContactForm from "./containers/ContactForm/ContactForm";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Contacts}/>
                <Route path="/contacts/add" component={ContactForm}/>
                <Route path="/contacts/edit/:id" component={ContactForm}/>
                <Route render={() => <h1>Hot found</h1>}/>
            </Switch>
        </Layout>
    );
  }
}

export default App;
